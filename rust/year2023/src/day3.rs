use std::collections::HashMap;

const MAP: [u32; 3] = [1, 10, 100];
pub fn problem_one() -> u32 {
    let input = std::fs::read_to_string("d3.txt").expect("Failed to find input file");
    let lines = input.lines().collect::<Vec<_>>();

    let width = lines[0].len();
    let height = lines.len();

    let mut numbers = Vec::new();
    let mut is_part_number = false;

    let mut nums = Vec::new();
    for y in 0..height {
        let line = lines[y].as_bytes();
        for x in 0..width {
            let byte = line[x];

            match byte as char {
                '0'..='9' => {
                    if !is_part_number {
                        is_part_number = check_at(&lines, x as isize, y as isize);
                    }
                    nums.push((byte as char).to_digit(10).unwrap());
                }
                _ => {
                    if nums.len() > 0 && nums[0] == 0 {
                        println!("Found 0!");
                    }
                    if is_part_number {
                        is_part_number = false;
                        let pn = nums
                            .iter()
                            .rev()
                            .enumerate()
                            .fold(0, |acc, (i, n)| acc + *n * MAP[i]);
                        numbers.push(pn);
                    }
                    nums.clear();
                }
            }
        }
    }

    numbers.iter().sum()
}

fn check_at(lines: &[&str], x: isize, y: isize) -> bool {
    let a = check_around(&lines, x - 1, y)
        || check_around(&lines, x + 1, y)
        || check_around(&lines, x, y - 1)
        || check_around(&lines, x, y + 1)
        || check_around(&lines, x - 1, y - 1)
        || check_around(&lines, x + 1, y - 1)
        || check_around(&lines, x - 1, y + 1)
        || check_around(&lines, x + 1, y + 1);
    a
}

fn check_around(lines: &[&str], x: isize, y: isize) -> bool {
    if x < 0 || x >= lines[0].len() as isize || y < 0 || y >= lines.len() as isize {
        return false;
    }

    let line = lines[y as usize].as_bytes();
    match line[x as usize] {
        b'0'..=b'9' => false,
        b'.' => false,
        _ => true,
    }
}

pub fn problem_two() -> usize {
    let input = std::fs::read_to_string("d3.txt").expect("Failed to find input file");
    let lines = input.lines().collect::<Vec<_>>();

    let width = lines[0].len();
    let height = lines.len();
    let mut is_gear = false;
    let mut gear_loc = (0, 0);
    let mut gears: HashMap<(isize, isize), Vec<usize>> = HashMap::new();

    let mut nums = Vec::new();

    for y in 0..height {
        let line = lines[y].as_bytes();
        for x in 0..width {
            let byte = line[x];

            match byte as char {
                '0'..='9' => {
                    if !is_gear {
                        (gear_loc, is_gear) = check_at_gears(&lines, x as isize, y as isize);
                    }
                    nums.push((byte as char).to_digit(10).unwrap());
                }
                _ => {
                    if nums.len() > 0 && nums[0] == 0 {
                        println!("Found 0!");
                    }
                    if is_gear {
                        is_gear = false;
                        let gr = nums
                            .iter()
                            .rev()
                            .enumerate()
                            .fold(0, |acc, (i, n)| acc + *n * MAP[i]);
                        let mut entry = gears.entry(gear_loc).or_default();
                        entry.push(gr as usize);
                    }
                    nums.clear();
                }
            }
        }
    }

    let mut total: usize = 0;
    for values in gears.values() {
        if values.len() == 2 {
            total += values.iter().fold(1, |acc, n| acc * *n);
        }
    }

    total
}

fn check_at_gears(lines: &[&str], x: isize, y: isize) -> ((isize, isize), bool) {
    let (loc, is_gear) = check_gears(&lines, x - 1, y);
    if is_gear {
        return (loc, true);
    }

    let (loc, is_gear) = check_gears(&lines, x + 1, y);
    if is_gear {
        return (loc, true);
    }

    let (loc, is_gear) = check_gears(&lines, x, y - 1);
    if is_gear {
        return (loc, true);
    }

    let (loc, is_gear) = check_gears(&lines, x, y + 1);
    if is_gear {
        return (loc, true);
    }

    let (loc, is_gear) = check_gears(&lines, x - 1, y - 1);
    if is_gear {
        return (loc, true);
    }

    let (loc, is_gear) = check_gears(&lines, x + 1, y - 1);
    if is_gear {
        return (loc, true);
    }

    let (loc, is_gear) = check_gears(&lines, x - 1, y + 1);
    if is_gear {
        return (loc, true);
    }

    let (loc, is_gear) = check_gears(&lines, x + 1, y + 1);
    if is_gear {
        return (loc, true);
    } else {
        return (loc, false);
    }
}

fn check_gears(lines: &[&str], x: isize, y: isize) -> ((isize, isize), bool) {
    if x < 0 || x >= lines[0].len() as isize || y < 0 || y >= lines.len() as isize {
        return ((x, y), false);
    }

    let line = lines[y as usize].as_bytes();
    match line[x as usize] {
        b'0'..=b'9' => ((x, y), false),
        b'.' => ((x, y), false),
        b'*' => ((x, y), true),
        _ => ((x, y), false),
    }
}
