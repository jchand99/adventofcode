use std::collections::HashMap;

pub fn problem_one() -> usize {
    let input = std::fs::read_to_string("d8.txt").expect("Failed to find input file.");
    let movements = input.lines().nth(0).unwrap().chars().collect::<Vec<_>>();

    let mut directions: HashMap<&str, Vec<&str>> = HashMap::new();
    input.lines().skip(2).for_each(|l| {
        let a = l.split(" = ").collect::<Vec<_>>();
        let key = a[0];
        let values = a[1][1..a[1].len() - 1].split(", ").collect::<Vec<_>>();

        directions.insert(key, values);
    });

    let mut current_place = "AAA";
    let mut current_step = 0;
    while current_place != "ZZZ" {
        let movement = movements[current_step % movements.len()];
        let entry = directions.entry(current_place).or_default();

        match movement {
            'L' => current_place = entry[0],
            'R' => current_place = entry[1],
            _ => panic!("Invalid movement!"),
        }
        current_step += 1;
    }

    current_step
}

// TOO SLOW
pub fn problem_two() -> usize {
    let input = std::fs::read_to_string("d8.txt").expect("Failed to find input file.");
    let movements = input.lines().nth(0).unwrap().chars().collect::<Vec<_>>();

    let mut starting_directions = Vec::new();
    let mut directions: HashMap<&str, Vec<&str>> = HashMap::new();
    input.lines().skip(2).for_each(|l| {
        let a = l.split(" = ").collect::<Vec<_>>();

        let key = a[0];
        if key.ends_with('A') {
            starting_directions.push(a[0]);
        }

        let values = a[1][1..a[1].len() - 1].split(", ").collect::<Vec<_>>();
        directions.insert(key, values);
    });

    // println!("{:?}", starting_directions);
    let len = starting_directions.len();
    let mut current_steps = vec![0; len];
    for (i, direction) in starting_directions.iter().enumerate() {
        let mut curr_dir = *direction;

        while !curr_dir.ends_with('Z') {
            let movement = movements[current_steps[i] % movements.len()];
            let entry = directions.get(curr_dir).unwrap();

            curr_dir = match movement {
                'L' => entry[0],
                'R' => entry[1],
                _ => panic!("Invalid movement!"),
            };

            current_steps[i] += 1;
        }
    }

    println!("{:?}", current_steps);

    let mut max = usize::MIN;
    for &steps in &current_steps {
        if steps > max {
            max = steps;
        }
    }
    let mut factors = vec![0.; len];
    for (i, steps) in current_steps.iter().enumerate() {
        factors[i] = max as f64 / *steps as f64;
    }

    for (i, steps) in current_steps.iter().enumerate() {
        println!("{:?}", *steps as f64 * factors[i]);
    }

    println!("{:?}", factors);

    let mut ans = current_steps.clone();
    // let mut found = false;
    // while !found {
    //     let mut smallest = usize::MAX;
    //     let mut smallest_idx = 0;
    //     for (i, &x) in ans.iter().enumerate() {
    //         if x < smallest {
    //             smallest = x;
    //             smallest_idx = i;
    //         }
    //     }

    //     ans[smallest_idx] += current_steps[smallest_idx];
    //     let first = ans[0];
    //     println!("{:?}", ans);
    //     found = ans.iter().all(|n| *n == first);
    //     // println!("{:?}", ans);
    // }

    ans[0]
}
