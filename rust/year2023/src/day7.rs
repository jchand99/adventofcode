use std::cmp::Ordering;

#[derive(Debug, Clone)]
struct Hand {
    cards: Vec<usize>,
    bid: usize,
}

pub fn problem_one() -> usize {
    let mut type_map = vec![0; b'U' as usize];
    type_map[b'A' as usize] = 13;
    type_map[b'K' as usize] = 12;
    type_map[b'Q' as usize] = 11;
    type_map[b'J' as usize] = 10;
    type_map[b'T' as usize] = 9;
    type_map[b'9' as usize] = 8;
    type_map[b'8' as usize] = 7;
    type_map[b'7' as usize] = 6;
    type_map[b'6' as usize] = 5;
    type_map[b'5' as usize] = 4;
    type_map[b'4' as usize] = 3;
    type_map[b'3' as usize] = 2;
    type_map[b'2' as usize] = 1;

    let input = std::fs::read_to_string("d7.txt").expect("Failed to find input file.");

    let mut sets: Vec<Vec<Hand>> = vec![vec![]; 7];
    for line in input.lines() {
        let line = line.split_whitespace().collect::<Vec<_>>();
        let bid = line[1].parse::<usize>().unwrap();

        let mut map = vec![0; b'U' as usize];
        for char in line[0].chars() {
            map[char as usize] += 1;
        }

        let map = map.iter().filter(|n| **n != 0).collect::<Vec<_>>();

        let hand = Hand {
            cards: line[0].chars().map(|c| type_map[c as usize]).collect(),
            bid,
        };

        match map.len() {
            3 => {
                let c = map.iter().fold(1, |acc, n| acc * **n);
                match c {
                    // three of a kind
                    3 => sets[3].push(hand),
                    // two pair
                    4 => sets[4].push(hand),
                    _ => {}
                }
            }
            // high cards
            5 => sets[6].push(hand),
            // one pair
            4 => sets[5].push(hand),
            2 => {
                let c = map.iter().fold(1, |acc, n| acc * **n);
                match c {
                    // full house
                    6 => sets[2].push(hand),
                    // four of a kind
                    4 => sets[1].push(hand),
                    _ => {}
                }
            }
            // five of a kind
            1 => sets[0].push(hand),
            _ => {}
        }
    }

    for set in &mut sets {
        set.sort_unstable_by(|a, b| {
            for (&aa, &bb) in a.cards.iter().zip(b.cards.iter()) {
                if aa < bb {
                    return Ordering::Less;
                } else if aa > bb {
                    return Ordering::Greater;
                }
            }

            return Ordering::Equal;
        });
    }

    let mut rank = 1;
    let mut total = 0;
    for set in sets.iter().rev() {
        for hand in set {
            // println!("{} * {} = {}", hand.bid, rank, rank * hand.bid);
            total += rank * hand.bid;
            rank += 1;
        }
    }

    total
}

pub fn problem_two() -> usize {
    let mut type_map = vec![0; b'U' as usize];
    type_map[b'A' as usize] = 13;
    type_map[b'K' as usize] = 12;
    type_map[b'Q' as usize] = 11;
    type_map[b'J' as usize] = 0;
    type_map[b'T' as usize] = 9;
    type_map[b'9' as usize] = 8;
    type_map[b'8' as usize] = 7;
    type_map[b'7' as usize] = 6;
    type_map[b'6' as usize] = 5;
    type_map[b'5' as usize] = 4;
    type_map[b'4' as usize] = 3;
    type_map[b'3' as usize] = 2;
    type_map[b'2' as usize] = 1;

    let input = std::fs::read_to_string("d7.txt").expect("Failed to find input file.");

    let mut sets: Vec<Vec<Hand>> = vec![vec![]; 7];
    for (lineidx, line) in input.lines().enumerate() {
        let line = line.split_whitespace().collect::<Vec<_>>();
        let bid = line[1].parse::<usize>().unwrap();
        let hand = Hand {
            cards: line[0].chars().map(|c| type_map[c as usize]).collect(),
            bid,
        };

        let mut map = vec![0; b'U' as usize];
        for char in line[0].chars() {
            map[char as usize] += 1;
        }

        // Account for J being stronger card
        if map[b'J' as usize] > 0 {
            let mut max = i32::MIN;
            for (idx, &n) in map.iter().enumerate() {
                if n > max && idx != b'J' as usize {
                    max = n;
                }
            }

            if max > 0 {
                let idx = map.iter().position(|&n| n == max).unwrap();
                map[idx] += map[b'J' as usize];
                map[b'J' as usize] = 0;
            }
        }
        // Account for J being stronger card

        let map = map.iter().filter(|n| **n != 0).collect::<Vec<_>>();

        // println!("{:?}", map);
        match map.len() {
            3 => {
                // println!("{}: {:?}, {:?}", lineidx + 1, hand, map);
                let c = map.iter().fold(1, |acc, n| acc * **n);
                match c {
                    // three of a kind
                    3 | 1 => sets[3].push(hand),
                    // two pair
                    4 => sets[4].push(hand),
                    _ => panic!("Invalid card configuration! 3:{c}"),
                }
            }
            // high cards
            5 => sets[6].push(hand),
            // one pair
            4 => sets[5].push(hand),
            2 => {
                let c = map.iter().fold(1, |acc, n| acc * **n);
                // println!("{}", c);
                match c {
                    // full house
                    6 => sets[2].push(hand),
                    // four of a kind
                    4 | 2 => sets[1].push(hand),
                    _ => panic!("Invalid card configuration 2:{c}"),
                }
            }
            // five of a kind
            1 => sets[0].push(hand),
            _ => panic!("Invalid card configuration"),
        }
    }

    println!("{:#?}", sets);

    for set in &mut sets {
        set.sort_unstable_by(|a, b| {
            for (&aa, &bb) in a.cards.iter().zip(b.cards.iter()) {
                if aa < bb {
                    return Ordering::Less;
                } else if aa > bb {
                    return Ordering::Greater;
                }
            }

            return Ordering::Equal;
        });
    }

    let mut rank = 1;
    let mut total = 0;
    for set in sets.iter().rev() {
        for hand in set {
            // println!("{} * {} = {}", hand.bid, rank, rank * hand.bid);
            total += rank * hand.bid;
            rank += 1;
        }
    }

    total
}
