use std::cmp::Ordering;

pub fn problem_one() -> usize {
    let input = std::fs::read_to_string("d4.txt").expect("Failed to find input file");

    let mut total = 0;

    for line in input.lines() {
        let numbers = line.split(": ").collect::<Vec<_>>()[1];
        let numbers = numbers.split(" | ").collect::<Vec<_>>();
        let mut winners = numbers[0]
            .split_whitespace()
            .map(|n| n.parse::<usize>().unwrap())
            .collect::<Vec<_>>();
        let mut elf_numbers = numbers[1]
            .split_whitespace()
            .map(|n| n.parse::<usize>().unwrap())
            .collect::<Vec<_>>();

        winners.sort_unstable();
        elf_numbers.sort_unstable();

        let mut i = 0;
        let mut j = 0;

        let mut matches = 0;
        while i < winners.len() && j < elf_numbers.len() {
            match winners[i].cmp(&elf_numbers[j]) {
                Ordering::Greater => j += 1,
                Ordering::Less => i += 1,
                Ordering::Equal => {
                    matches += 1;
                    i += 1;
                    j += 1;
                }
            }
        }

        let mut pts = match matches > 0 {
            true => 1,
            false => 0,
        };

        for _ in 1..matches {
            pts = pts * 2;
        }

        total += pts;
    }

    total
}

pub fn problem_two() -> usize {
    let input = std::fs::read_to_string("d4.txt").expect("Failed to find input file");
    let num_cards = input.lines().count();

    let mut map = vec![1; num_cards + 1];
    map[0] = 0;

    for (card_id, line) in input.lines().enumerate() {
        let card_id = card_id + 1;

        let numbers = line.split(": ").collect::<Vec<_>>()[1];
        let numbers = numbers.split(" | ").collect::<Vec<_>>();
        let mut winners = numbers[0]
            .split_whitespace()
            .map(|n| n.parse::<usize>().unwrap())
            .collect::<Vec<_>>();
        let mut elf_numbers = numbers[1]
            .split_whitespace()
            .map(|n| n.parse::<usize>().unwrap())
            .collect::<Vec<_>>();

        winners.sort_unstable();
        elf_numbers.sort_unstable();

        let mut i = 0;
        let mut j = 0;

        let mut matches = 0;
        while i < winners.len() && j < elf_numbers.len() {
            match winners[i].cmp(&elf_numbers[j]) {
                Ordering::Greater => j += 1,
                Ordering::Less => i += 1,
                Ordering::Equal => {
                    matches += 1;
                    i += 1;
                    j += 1;
                }
            }
        }

        let mut idx = card_id + 1;
        let card_count = map[card_id];
        while matches > 0 {
            map[idx] += card_count;
            idx += 1;
            matches -= 1;
        }
    }

    map.iter().sum()
}
