pub fn problem_one() -> u64 {
    let input = std::fs::read_to_string("d1.txt").expect("Failed to find input file");
    let lines = input.lines();

    let mut nums: Vec<u64> = Vec::new();
    for line in lines {
        let line = line.as_bytes();

        let mut i = 0;
        let mut j = line.len() - 1;

        let mut left_num: u64 = 0;
        let mut right_num: u64 = 0;
        while left_num == 0 || right_num == 0 {
            let left_c = line[i] as char;
            let right_c = line[j] as char;

            if left_c.is_digit(10) {
                left_num = left_c.to_digit(10).unwrap() as u64;
            } else {
                i += 1;
            }

            if right_c.is_digit(10) {
                right_num = right_c.to_digit(10).unwrap() as u64;
            } else {
                j -= 1;
            }
        }
        nums.push((left_num * 10) + right_num);
    }

    return nums.iter().sum();
}

pub fn problem_two() -> u64 {
    let input = std::fs::read_to_string("d1.txt").expect("Failed to find input file");
    let lines = input.lines();

    let mut nums: Vec<u64> = Vec::new();
    for line in lines {
        let line_b = line.as_bytes();

        let mut i = 0;
        let mut j = line_b.len() - 1;

        let mut left_num: u64 = 0;
        let mut right_num: u64 = 0;
        while (left_num == 0 || right_num == 0) && i <= j {
            let left_c = line_b[i] as char;
            let right_c = line_b[j] as char;

            // LEFT CHAR
            if left_c.is_digit(10) {
                left_num = left_c.to_digit(10).unwrap() as u64;
            } else {
                match check_left_word_num(left_c, i, &line, &line_b) {
                    Some(n) => left_num = n,
                    None => i += 1,
                }
            }

            // RIGHT CHAR
            if right_c.is_digit(10) {
                right_num = right_c.to_digit(10).unwrap() as u64;
            } else {
                match check_right_word_num(right_c, j, &line, &line_b) {
                    Some(n) => right_num = n,
                    None => j -= 1,
                }
            }
        }
        // println!("{}{} | i: {}, j: {}", left_num, right_num, i, j);
        nums.push((left_num * 10) + right_num);
    }

    return nums.iter().sum();
}

fn check_left_word_num(left_c: char, i: usize, line: &str, line_b: &[u8]) -> Option<u64> {
    let line_len = line.len();
    match left_c {
        'o' if i + 3 < line_len => {
            if &line[i..i + 3] == "one" {
                Some(1)
            } else {
                None
            }
        }
        't' if i + 1 < line_len => match line_b[i + 1] {
            b'w' if i + 3 < line_len => {
                if &line[i..i + 3] == "two" {
                    Some(2)
                } else {
                    None
                }
            }
            b'h' if i + 5 < line_len => {
                if &line[i..i + 5] == "three" {
                    Some(3)
                } else {
                    None
                }
            }
            _ => None,
        },
        'f' if i + 1 < line_len => match line_b[i + 1] {
            b'i' if i + 4 < line_len => {
                if &line[i..i + 4] == "five" {
                    Some(5)
                } else {
                    None
                }
            }
            b'o' if i + 4 < line_len => {
                if &line[i..i + 4] == "four" {
                    Some(4)
                } else {
                    None
                }
            }
            _ => None,
        },
        's' if i + 1 < line_len => match line_b[i + 1] {
            b'i' if i + 3 < line_len => {
                if &line[i..i + 3] == "six" {
                    Some(6)
                } else {
                    None
                }
            }
            b'e' if i + 5 < line_len => {
                if &line[i..i + 5] == "seven" {
                    Some(7)
                } else {
                    None
                }
            }
            _ => None,
        },
        'e' if i + 5 < line_len => {
            if &line[i..i + 5] == "eight" {
                Some(8)
            } else {
                None
            }
        }
        'n' if i + 4 < line_len => {
            if &line[i..i + 4] == "nine" {
                Some(9)
            } else {
                None
            }
        }
        _ => None,
    }
}

fn check_right_word_num(right_c: char, j: usize, line: &str, line_b: &[u8]) -> Option<u64> {
    let jj = j as isize;
    match right_c {
        'e' if jj - 1 > 0 => match line_b[j - 1] {
            b'n' if jj - 2 > 0 => match line_b[j - 2] {
                b'o' => Some(1),
                b'i' if jj - 3 > 0 => {
                    if &line[(j - 3)..=j] == "nine" {
                        Some(9)
                    } else {
                        None
                    }
                }
                _ => None,
            },
            b'e' if jj - 4 > 0 => {
                if &line[(j - 4)..=j] == "three" {
                    Some(3)
                } else {
                    None
                }
            }
            b'v' if jj - 3 > 0 => {
                if &line[(j - 3)..=j] == "five" {
                    Some(5)
                } else {
                    None
                }
            }
            _ => None,
        },
        'o' if jj - 2 > 0 => {
            if &line[(j - 2)..=j] == "two" {
                Some(2)
            } else {
                None
            }
        }
        'r' if jj - 3 > 0 => {
            if &line[(j - 3)..=j] == "four" {
                Some(4)
            } else {
                None
            }
        }
        'x' if jj - 2 > 0 => {
            if &line[(j - 2)..=j] == "six" {
                Some(6)
            } else {
                None
            }
        }
        'n' if jj - 4 > 0 => {
            if &line[(j - 4)..=j] == "seven" {
                Some(7)
            } else {
                None
            }
        }
        't' if jj - 4 > 0 => {
            if &line[(j - 4)..=j] == "eight" {
                Some(8)
            } else {
                None
            }
        }
        _ => None,
    }
}
