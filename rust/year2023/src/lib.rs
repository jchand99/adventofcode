#![allow(dead_code)]

mod day1;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;
mod day7;
mod day8;

#[test]
fn day1() {
    assert_eq!(day1::problem_one(), 55607);
    assert_eq!(day1::problem_two(), 55291);
}

#[test]
fn day2() {
    assert_eq!(day2::problem_one(), 2204);
    assert_eq!(day2::problem_two(), 71036);
}

#[test]
fn day3() {
    assert_eq!(day3::problem_one(), 531561);
    assert_eq!(day3::problem_two(), 83279367);
}

#[test]
fn day4() {
    assert_eq!(day4::problem_one(), 26218);
    assert_eq!(day4::problem_two(), 9997537);
}

#[test]
fn day5() {
    //assert_eq!(day5::problem_one(), 462648397);
    // :(
    //assert_eq!(day5::problem_two(), 9997537);
}

#[test]
fn day6() {
    assert_eq!(day6::problem_one(), 140220);
    assert_eq!(day6::problem_two(), 39570185);
}

#[test]
fn day7() {
    assert_eq!(day7::problem_one(), 250120186);
    assert_eq!(day7::problem_two(), 250665248);
}

#[test]
fn day8() {
    // assert_eq!(day8::problem_one(), 17873);
    assert_eq!(day8::problem_two(), 250665248);
}
