pub fn problem_one() -> usize {
    const MAX_RED_CUBES: usize = 12;
    const MAX_BLUE_CUBES: usize = 14;
    const MAX_GREEN_CUBES: usize = 13;

    let input = std::fs::read_to_string("d2.txt").expect("Failed to find input file");

    let mut valid_ids = Vec::new();
    for line in input.lines() {
        let game_split = line.split(":").collect::<Vec<_>>();
        let game_id: usize = game_split[0].split_whitespace().collect::<Vec<_>>()[1]
            .parse()
            .unwrap();

        valid_ids.push(game_id);

        let cubes = game_split[1]
            .split(";")
            .flat_map(|s| s.split(",").collect::<Vec<_>>())
            .map(|s| s.trim())
            .collect::<Vec<_>>();

        for c in cubes {
            let a = c.split_whitespace().collect::<Vec<_>>();
            let n_cubes = a[0].parse::<usize>().unwrap();

            match a[1] {
                "blue" => {
                    if n_cubes > MAX_BLUE_CUBES {
                        valid_ids.pop();
                        break;
                    }
                }
                "red" => {
                    if n_cubes > MAX_RED_CUBES {
                        valid_ids.pop();
                        break;
                    }
                }
                "green" => {
                    if n_cubes > MAX_GREEN_CUBES {
                        valid_ids.pop();
                        break;
                    }
                }
                _ => panic!("Invalid cube color!"),
            }
        }
    }

    valid_ids.iter().sum()
}

pub fn problem_two() -> usize {
    let input = std::fs::read_to_string("d2.txt").expect("Failed to find input file");

    let mut power_sets = Vec::new();
    for line in input.lines() {
        let mut max_red_cubes = usize::MIN;
        let mut max_green_cubes = usize::MIN;
        let mut max_blue_cubes = usize::MIN;

        let game_split = line.split(":").collect::<Vec<_>>();

        let cubes = game_split[1]
            .split(";")
            .flat_map(|s| s.split(",").collect::<Vec<_>>())
            .map(|s| s.trim())
            .collect::<Vec<_>>();

        for c in cubes {
            let a = c.split_whitespace().collect::<Vec<_>>();
            let n_cubes = a[0].parse::<usize>().unwrap();

            match a[1] {
                "blue" => {
                    if n_cubes > max_blue_cubes {
                        max_blue_cubes = n_cubes;
                    }
                }
                "red" => {
                    if n_cubes > max_red_cubes {
                        max_red_cubes = n_cubes;
                    }
                }
                "green" => {
                    if n_cubes > max_green_cubes {
                        max_green_cubes = n_cubes;
                    }
                }
                _ => panic!("Invalid cube color!"),
            }
        }
        power_sets.push(max_red_cubes * max_green_cubes * max_blue_cubes);
    }
    power_sets.iter().sum()
}
