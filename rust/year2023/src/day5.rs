#[derive(Debug, Clone, Copy)]
struct Map {
    pub dest: u64,
    pub source: u64,
    pub range: u64,
}

pub fn problem_one() -> u64 {
    let input = std::fs::read_to_string("d5.txt").expect("Failed to find input file");
    let seeds = input
        .lines()
        .nth(0)
        .unwrap()
        .split(": ")
        .nth(1)
        .unwrap()
        .split_whitespace()
        .map(|n| n.parse::<u64>().unwrap())
        .collect::<Vec<u64>>();

    println!("Seeds: {:?}", seeds);

    let mut seed_to_soil_maps: Vec<Map> = Vec::new();
    let mut soil_to_fertilizer_maps: Vec<Map> = Vec::new();
    let mut fertilizer_to_water_maps: Vec<Map> = Vec::new();
    let mut water_to_light_maps: Vec<Map> = Vec::new();
    let mut light_to_temperature_maps: Vec<Map> = Vec::new();
    let mut temperature_to_humidity_maps: Vec<Map> = Vec::new();
    let mut humidity_to_location_maps: Vec<Map> = Vec::new();

    let mut map_type = "";
    for line in input.lines().skip(1).filter(|l| !l.is_empty()) {
        if !line.as_bytes()[0].is_ascii_digit() {
            map_type = line.split_whitespace().nth(0).unwrap();
            continue;
        }
        // println!("{:?}", map_type);

        let values = line
            .split_whitespace()
            .map(|n| n.parse::<u64>().unwrap())
            .collect::<Vec<u64>>();

        let m = Map {
            dest: values[0],
            source: values[1],
            range: values[2],
        };

        match map_type {
            "seed-to-soil" => seed_to_soil_maps.push(m),
            "soil-to-fertilizer" => soil_to_fertilizer_maps.push(m),
            "fertilizer-to-water" => fertilizer_to_water_maps.push(m),
            "water-to-light" => water_to_light_maps.push(m),
            "light-to-temperature" => light_to_temperature_maps.push(m),
            "temperature-to-humidity" => temperature_to_humidity_maps.push(m),
            "humidity-to-location" => humidity_to_location_maps.push(m),
            _ => panic!("Invalid map type!"),
        }
    }

    let mut smallest_value = u64::MAX;
    let mut smallest_seed = 0;
    for seed in seeds {
        let soil_value = find_value_in_map(&seed_to_soil_maps, seed);
        let fertilizer_value = find_value_in_map(&soil_to_fertilizer_maps, soil_value);
        let water_value = find_value_in_map(&fertilizer_to_water_maps, fertilizer_value);
        let light_value = find_value_in_map(&water_to_light_maps, water_value);
        let temperature_value = find_value_in_map(&light_to_temperature_maps, light_value);
        let humidity_value = find_value_in_map(&temperature_to_humidity_maps, temperature_value);
        let location_value = find_value_in_map(&humidity_to_location_maps, humidity_value);

        if location_value < smallest_value {
            smallest_value = location_value;
            smallest_seed = seed;
        }
    }

    println!("{:?}", smallest_seed);
    smallest_value
}

fn find_value_in_map(maps: &[Map], value: u64) -> u64 {
    for map in maps {
        if value >= map.source && value <= (map.source + map.range) {
            return map.dest + (value - map.source);
        }
    }

    return value;
}

#[derive(Debug, Clone, Copy)]
struct SeedRange {
    start: u64,
    end: u64,
    range: u64,
}

impl SeedRange {
    fn contains(&self, seed: u64) -> bool {
        seed <= self.end && seed >= self.start
    }
}

pub fn problem_two() -> u64 {
    let input = std::fs::read_to_string("d5.txt").expect("Failed to find input file");
    let seeds_nums = input
        .lines()
        .nth(0)
        .unwrap()
        .split(": ")
        .nth(1)
        .unwrap()
        .split_whitespace()
        .map(|n| n.parse::<u64>().unwrap())
        .collect::<Vec<u64>>();

    let seeds = seeds_nums
        .iter()
        .step_by(2)
        .zip(seeds_nums.iter().skip(1).step_by(2))
        .map(|(&source, &range)| SeedRange {
            start: source,
            end: source + range,
            range,
        })
        .collect::<Vec<SeedRange>>();

    let mut maps: Vec<Vec<Map>> = vec![vec![]; 7];

    let mut map_type = "";
    for line in input.lines().skip(1).filter(|l| !l.is_empty()) {
        println!("{:?}", line);
        if !line.as_bytes()[0].is_ascii_digit() {
            map_type = line.split_whitespace().nth(0).unwrap();
            continue;
        }

        let values = line
            .split_whitespace()
            .map(|n| n.parse::<u64>().unwrap())
            .collect::<Vec<u64>>();

        let m = Map {
            dest: values[0],
            source: values[1],
            range: values[2],
        };

        match map_type {
            "seed-to-soil" => maps[0].push(m),
            "soil-to-fertilizer" => maps[1].push(m),
            "fertilizer-to-water" => maps[2].push(m),
            "water-to-light" => maps[3].push(m),
            "light-to-temperature" => maps[4].push(m),
            "temperature-to-humidity" => maps[5].push(m),
            "humidity-to-location" => maps[6].push(m),
            _ => panic!("Invalid map type!"),
        }
    }

    // println!("{:?}", maps.iter().map(|m| m.len()).collect::<Vec<_>>());

    // let mut points = vec![];
    // for m in maps.iter().rev() {
    //     let mut translated_points = points
    //         .iter()
    //         .map(|&p| {
    //             m.iter()
    //                 .find_map(|&e| {
    //                     if p >= e.dest && p < e.dest + e.range {
    //                         let offset = p - e.dest;
    //                         Some(e.source + offset)
    //                     } else {
    //                         None
    //                     }
    //                 })
    //                 .unwrap_or(p)
    //         })
    //         .collect::<Vec<_>>();
    //     let mut new_points = m.iter().map(|e| e.source).collect::<Vec<_>>();
    //     translated_points.append(&mut new_points);

    //     points = translated_points;
    // }

    // println!("{:?}", points);

    // let points = points
    //     .iter()
    //     .filter(|&&p| seeds.iter().any(|&s| s.contains(p)))
    //     .collect::<Vec<_>>();

    let mut smallest_value = u64::MAX;
    for seed in seeds {
        for s in seed.start..=((seed.start + seed.range) - 1) {
            let soil_value = find_value_in_map(&maps[0], s);
            let fertilizer_value = find_value_in_map(&maps[1], soil_value);
            let water_value = find_value_in_map(&maps[2], fertilizer_value);
            let light_value = find_value_in_map(&maps[3], water_value);
            let temperature_value = find_value_in_map(&maps[4], light_value);
            let humidity_value = find_value_in_map(&maps[5], temperature_value);
            let location_value = find_value_in_map(&maps[6], humidity_value);

            if location_value < smallest_value {
                smallest_value = location_value;
            }
        }
    }

    smallest_value
}
//[MapRange { dest: Range { start: 0, end: 37 },  source: Range { start: 15, end: 52 } },
// MapRange { dest: Range { start: 37, end: 39 }, source: Range { start: 52, end: 54 } },
// MapRange { dest: Range { start: 39, end: 54 }, source: Range { start: 0, end: 15 } }]

//[Range { start: 52, end: 100 }]
// fn find_range_in_map(maps: &[MapRange], seed_range: SeedRange) -> Vec<SeedRange> {
//     println!("Maps: {:?}\nSeed: {:?}", maps, seed_range);
//     let mut ranges = Vec::new();

//     for &map in maps {
//         println!("Map: {:?}", map);
//         // If the range is within the entire map range
//         if seed_range.start >= map.source.start && seed_range.end <= map.source.end {
//             return vec![map.dest];
//         } else if seed_range.start >= map.source.end && seed_range.end >= map.source.end {
//             ranges.push(map.dest);
//             find_range_in_map(
//                 maps,
//                 SeedRange {
//                     start: map.source.end + 1,
//                     end: seed_range.end,
//                 },
//             );
//         } else if seed_range.end >= map.source.start && seed_range.start <= map.source.start {
//             find_range_in_map(
//                 maps,
//                 SeedRange {
//                     start: seed_range.start,
//                     end: map.source.start,
//                 },
//             );
//         } else {
//             continue;
//         }
//     }

//     return ranges;
// }

// 2520480
// 2520480
