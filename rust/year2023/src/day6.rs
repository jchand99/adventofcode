pub fn problem_one() -> usize {
    let input = std::fs::read_to_string("d6.txt").expect("Failed to find input file.");
    let lines = input.lines().collect::<Vec<_>>();

    let times = lines[0]
        .split_whitespace()
        .skip(1)
        .map(|n| n.parse::<usize>().unwrap())
        .collect::<Vec<_>>();
    let distances = lines[1]
        .split_whitespace()
        .skip(1)
        .map(|n| n.parse::<usize>().unwrap())
        .collect::<Vec<_>>();

    assert!(times.len() == distances.len());

    let mut total = 1;
    for (timeidx, &time) in times.iter().enumerate() {
        let mut i = 0;
        let mut j = time;

        let mut t = 0;
        while i != time && j != 0 {
            if i * j > distances[timeidx] {
                t += 1;
            }
            i += 1;
            j -= 1;
        }

        total *= t;
    }

    total
}

pub fn problem_two() -> usize {
    let input = std::fs::read_to_string("d6.txt").expect("Failed to find input file.");
    let lines = input.lines().collect::<Vec<_>>();

    let time = lines[0]
        .split_whitespace()
        .skip(1)
        .flat_map(|n| n.chars())
        .collect::<String>()
        .parse::<usize>()
        .unwrap();
    let distance = lines[1]
        .split_whitespace()
        .skip(1)
        .flat_map(|n| n.chars())
        .collect::<String>()
        .parse::<usize>()
        .unwrap();

    println!("{}, {}", time, distance);

    let mut total = 0;
    let mut i = 0;
    let mut j = time;

    while i < j {
        let n = i * j;
        if n > distance {
            total += 1;
        }
        i += 1;
        j -= 1;
    }

    (total * 2) + 1
}
