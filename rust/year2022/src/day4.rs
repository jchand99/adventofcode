pub fn problem_one() -> i32 {
    let input = std::fs::read_to_string("d4.txt").expect("Failed to find input file");
    let total = input
        .lines()
        .map(|l| l.split(",").collect::<Vec<_>>())
        .fold(0, |acc, splits| {
            let l = splits[0]
                .split("-")
                .map(|s| s.parse::<u64>().unwrap())
                .collect::<Vec<_>>();
            let r = splits[1]
                .split("-")
                .map(|s| s.parse::<u64>().unwrap())
                .collect::<Vec<_>>();

            if (l[0] <= r[0] && l[1] >= r[1]) || (r[0] <= l[0] && r[1] >= l[1]) {
                acc + 1
            } else {
                acc
            }
        });
    total
}

pub fn problem_two() -> i32 {
    let input = std::fs::read_to_string("d4.txt").expect("Failed to find input file");
    let total = input
        .lines()
        .map(|l| l.split(",").collect::<Vec<_>>())
        .fold(0, |acc, splits| {
            let l = splits[0]
                .split("-")
                .map(|s| s.parse::<u64>().unwrap())
                .collect::<Vec<_>>();
            let r = splits[1]
                .split("-")
                .map(|s| s.parse::<u64>().unwrap())
                .collect::<Vec<_>>();

            let mut a = [&l, &r];
            a.sort();
            let [l, r] = a;

            if l[1] >= r[0] {
                acc + 1
            } else {
                acc
            }
        });
    total
}
