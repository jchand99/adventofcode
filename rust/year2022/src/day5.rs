pub fn problem_one() -> String {
    let input = std::fs::read_to_string("d5.txt").expect("Failed to find input file");
    let mut rows = input
        .lines()
        .map(|line| {
            if !line.starts_with("move") {
                line.chars().skip(1).step_by(4).collect::<Vec<_>>()
            } else {
                vec![]
            }
        })
        .filter(|x| !x.is_empty())
        .collect::<Vec<_>>();
    let labels = rows.pop().unwrap();
    let mut crates: Vec<Vec<char>> = Vec::with_capacity(labels.len());

    for _ in 0..labels.len() {
        crates.push(vec![]);
    }

    rows.iter_mut().rev().for_each(|layer| {
        layer.iter().enumerate().for_each(|(i, c)| {
            if (*c as usize) != 32 {
                crates[i].push(*c);
            }
        });
    });

    let commands = input
        .lines()
        .skip(rows.len() + 2)
        .map(|line| {
            line.split_whitespace()
                .skip(1)
                .step_by(2)
                .map(|n| n.parse::<usize>().unwrap())
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>();

    commands.iter().for_each(|command| {
        let amount = command[0];
        let from = command[1] - 1;
        let to = command[2] - 1;

        for _ in 0..amount {
            let moving = crates[from].pop().unwrap();
            crates[to].push(moving);
        }
    });

    let answer = crates
        .iter_mut()
        .map(|cr| cr.pop().unwrap())
        .fold(String::new(), |acc, c| format!("{}{}", acc, c));

    answer
}

pub fn problem_two() -> String {
    let input = std::fs::read_to_string("d5.txt").expect("Failed to find input file");
    let mut rows = input
        .lines()
        .map(|line| {
            if !line.starts_with("move") {
                line.chars().skip(1).step_by(4).collect::<Vec<_>>()
            } else {
                vec![]
            }
        })
        .filter(|x| !x.is_empty())
        .collect::<Vec<_>>();
    let labels = rows.pop().unwrap();
    let mut crates: Vec<Vec<char>> = Vec::with_capacity(labels.len());

    for _ in 0..labels.len() {
        crates.push(vec![]);
    }

    rows.iter_mut().rev().for_each(|layer| {
        layer.iter().enumerate().for_each(|(i, c)| {
            if (*c as usize) != 32 {
                crates[i].push(*c);
            }
        });
    });

    let commands = input
        .lines()
        .skip(rows.len() + 2)
        .map(|line| {
            line.split_whitespace()
                .skip(1)
                .step_by(2)
                .map(|n| n.parse::<usize>().unwrap())
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>();

    commands.iter().for_each(|command| {
        let amount = command[0];
        let from = command[1] - 1;
        let to = command[2] - 1;

        let mut to_move = Vec::with_capacity(amount);
        for _ in 0..amount {
            let moving = crates[from].pop().unwrap();
            to_move.push(moving);
        }
        for _ in 0..amount {
            let value = to_move.pop().unwrap();
            crates[to].push(value);
        }
    });

    let answer = crates
        .iter_mut()
        .map(|cr| cr.pop().unwrap())
        .fold(String::new(), |acc, c| format!("{}{}", acc, c));
    answer
}
