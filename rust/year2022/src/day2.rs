pub fn problem_one() -> i32 {
    let input = std::fs::read_to_string("d2.txt").expect("Failed to find input file");

    let mut total = 0;
    for line in input.lines() {
        let round = line.split(" ").collect::<Vec<_>>();

        let opponent = round[0];
        let me = round[1];

        match opponent {
            // Rock v Rock
            "A" if me == "X" => {
                total += 1 + 3;
            }
            // Rock v Paper
            "A" if me == "Y" => {
                total += 2 + 6;
            }
            // Rock v scissor
            "A" if me == "Z" => {
                total += 3;
            }
            // Paper v Rock
            "B" if me == "X" => {
                total += 1 + 0;
            }
            // Paper v Paper
            "B" if me == "Y" => {
                total += 2 + 3;
            }
            // Paper v scissor
            "B" if me == "Z" => {
                total += 3 + 6;
            }
            // Scissor v Rock
            "C" if me == "X" => {
                total += 1 + 6;
            }
            // Scissor v Paper
            "C" if me == "Y" => {
                total += 2 + 0;
            }
            // Scissor v scissor
            "C" if me == "Z" => {
                total += 3 + 3;
            }
            _ => {}
        }
    }
    total
}

pub fn problem_two() -> i32 {
    let input = std::fs::read_to_string("d2.txt").expect("Failed to find input file");
    let mut total = 0;
    for line in input.lines() {
        let round = line.split(" ").collect::<Vec<_>>();

        let opponent = round[0];
        let me = round[1];

        match opponent {
            // Rock Lose -> Scissor
            "A" if me == "X" => {
                total += 3;
            }
            // Rock Draw -> Rock
            "A" if me == "Y" => {
                total += 1 + 3;
            }
            // Rock Win -> Paper
            "A" if me == "Z" => {
                total += 2 + 6;
            }
            // Paper Lose -> Rock
            "B" if me == "X" => {
                total += 1;
            }
            // Paper Draw -> Paper
            "B" if me == "Y" => {
                total += 2 + 3;
            }
            // Paper Win -> Scissor
            "B" if me == "Z" => {
                total += 3 + 6;
            }
            // Scissor Lose -> Paper
            "C" if me == "X" => {
                total += 2;
            }
            // Scissor Draw -> Scissor
            "C" if me == "Y" => {
                total += 3 + 3;
            }
            // Scissor Win -> Rock
            "C" if me == "Z" => {
                total += 1 + 6;
            }
            _ => {}
        }
    }
    total
}
