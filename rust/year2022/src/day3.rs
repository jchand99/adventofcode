pub fn problem_one() -> u32 {
    let input = std::fs::read_to_string("d3.txt").expect("Failed to find input file");
    let total = input
        .lines()
        .map(|line| line.split_at(line.len() / 2))
        .flat_map(|(left, right)| left.chars().find(|c| right.contains(*c)))
        .fold(0, |acc, c| {
            if c.is_ascii_lowercase() {
                acc + (c as u32) - 96
            } else {
                acc + (c as u32) - 38
            }
        });

    total
}

pub fn problem_two() -> u32 {
    let input = std::fs::read_to_string("d3.txt").expect("Failed to find input file");
    let total = input
        .lines()
        .step_by(3)
        .zip(
            input
                .lines()
                .skip(1)
                .step_by(3)
                .zip(input.lines().skip(2).step_by(3)),
        )
        .flat_map(|(first, (second, third))| {
            // TODO: this could possibly be optimized
            // by sorting the strings and iterating
            // over the smallest first.
            first
                .chars()
                .find(|c| second.contains(*c) && third.contains(*c))
        })
        .fold(0, |acc, c| {
            if c.is_ascii_lowercase() {
                acc + (c as u32) - 96
            } else {
                acc + (c as u32) - 38
            }
        });
    total
}
