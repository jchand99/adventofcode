mod day1;
mod day2;
mod day3;
mod day4;
mod day5;

#[test]
fn day1() {
    assert_eq!(day1::problem_one(), 72478);
    assert_eq!(day1::problem_two(), 210367);
}

#[test]
fn day2() {
    assert_eq!(day2::problem_one(), 11475);
    assert_eq!(day2::problem_two(), 16862);
}

#[test]
fn day3() {
    assert_eq!(day3::problem_one(), 8039);
    assert_eq!(day3::problem_two(), 2510);
}

#[test]
fn day4() {
    assert_eq!(day4::problem_one(), 441);
    assert_eq!(day4::problem_two(), 861);
}

#[test]
fn day5() {
    assert_eq!(day5::problem_one(), String::from("HBTMTBSDC"));
    assert_eq!(day5::problem_two(), String::from("PQTJRSHWS"));
}
