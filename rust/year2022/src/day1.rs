pub fn problem_one() -> u64 {
    let input = std::fs::read_to_string("d1.txt").expect("Failed to find input file");
    let mut max = 0;

    let mut count = 0;
    for line in input.lines() {
        match line.parse::<u64>() {
            Ok(calorie_count) => {
                count += calorie_count;
            }
            Err(_) => {
                if count > max {
                    max = count;
                }
                count = 0;
            }
        }
    }

    max
}

pub fn problem_two() -> u64 {
    let input = std::fs::read_to_string("d1.txt").expect("Failed to find input file");
    let mut elves = Vec::new();

    let mut count = 0;
    for line in input.lines() {
        match line.parse::<u64>() {
            Ok(calorie_count) => {
                count += calorie_count;
            }
            Err(_) => {
                elves.push(count);
                count = 0;
            }
        }
    }
    elves.push(count);

    elves.sort();
    elves.reverse();
    elves[0] + elves[1] + elves[2]
}
