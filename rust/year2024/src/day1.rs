use std::collections::BinaryHeap;

// UGLYYYY
pub fn problem_one() -> usize {
    let mut left_set = BinaryHeap::with_capacity(500);
    let mut right_set = BinaryHeap::with_capacity(500);

    std::fs::read_to_string("d1.txt")
        .expect("Input should be readable")
        .lines()
        .map(|line| line.split_whitespace())
        .for_each(|mut l| {
            left_set.push(l.next().unwrap().parse::<isize>().unwrap());
            right_set.push(l.next().unwrap().parse::<isize>().unwrap());
        });

    let mut sum = 0;
    for _ in 0..left_set.len() {
        sum += (left_set.pop().unwrap() - right_set.pop().unwrap()).abs();
    }

    sum as usize
}

// UGLY but better than pt1
pub fn problem_two() -> usize {
    let content = std::fs::read_to_string("d1.txt").expect("Input must be readable");

    let mut counts = vec![0; 100000];

    content
        .lines()
        .flat_map(|line| line.split_whitespace().skip(1).step_by(2).nth(0))
        .flat_map(|a| a.parse::<usize>())
        .for_each(|n| {
            counts[n] += 1;
        });

    let sum = content
        .lines()
        .flat_map(|line| line.split_whitespace().step_by(2).nth(0))
        .flat_map(|a| a.parse::<usize>())
        .fold(0, |acc, b| acc + (b * counts[b]));

    sum
}
