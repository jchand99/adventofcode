pub fn problem_one() -> usize {
    let content = std::fs::read_to_string("d3.txt")
        .expect("Input must be readable!")
        .bytes()
        .collect::<Vec<_>>();

    let sum = content
        .iter()
        .enumerate()
        .filter_map(|(i, _)| -> Option<(usize, usize)> {
            if i + 4 > content.len() {
                return None;
            }

            match &content[i..i + 4] {
                b"mul(" => {
                    let mut start = i + 4;
                    let mut loop_count = 0;
                    let mut first_num_chars = Vec::new();

                    while content[start] as char != ',' && loop_count <= 3 {
                        // println!("{:?}", content[start] as char);
                        first_num_chars.push(content[start].clone());
                        start += 1;
                        loop_count += 1;
                    }

                    let first_num_chars = std::str::from_utf8(&first_num_chars).unwrap();
                    // println!("First Num: {}", first_num_chars);
                    let first_num = match first_num_chars.parse::<usize>() {
                        Ok(n) => n,
                        Err(_) => return None,
                    };

                    if content[start] as char != ',' {
                        return None;
                    }

                    start += 1;
                    let mut second_num_chars = Vec::new();
                    loop_count = 0;
                    while content[start] != ')' as u8 && loop_count < 3 {
                        second_num_chars.push(content[start]);
                        start += 1;
                        loop_count += 1;
                    }

                    if content[start] as char != ')' {
                        return None;
                    }

                    let second_num_chars = std::str::from_utf8(&second_num_chars).unwrap();
                    // println!("Second Num: {}", second_num_chars);
                    let second_num = match second_num_chars.parse::<usize>() {
                        Ok(n) => n,
                        Err(_) => return None,
                    };

                    // println!("{:?} {:?}", first_num, second_num);
                    Some((first_num, second_num))
                }
                _ => None,
            }
        })
        .map(|(f, s)| f * s)
        .sum();

    sum
}

pub fn problem_two() -> usize {
    let content = std::fs::read_to_string("d3.txt")
        .expect("Input must be readable!")
        .bytes()
        .collect::<Vec<_>>();

    let mut enabled = true;
    let sum = content
        .iter()
        .enumerate()
        .filter_map(|(i, _)| -> Option<(usize, usize)> {
            if i + 4 > content.len() {
                return None;
            }

            match &content[i..i + 4] {
                b"do()" => {
                    enabled = true;
                    None
                }
                // Ugly but it works lol
                b"don'" => {
                    if &content[i..i + 7] == b"don't()" {
                        enabled = false;
                    }
                    None
                }
                b"mul(" if enabled => {
                    let mut start = i + 4;
                    let mut loop_count = 0;
                    let mut first_num_chars = Vec::new();

                    while content[start] as char != ',' && loop_count <= 3 {
                        first_num_chars.push(content[start].clone());
                        start += 1;
                        loop_count += 1;
                    }

                    let first_num_chars = std::str::from_utf8(&first_num_chars).unwrap();
                    let first_num = match first_num_chars.parse::<usize>() {
                        Ok(n) => n,
                        Err(_) => return None,
                    };

                    if content[start] as char != ',' {
                        return None;
                    }

                    start += 1;
                    let mut second_num_chars = Vec::new();
                    loop_count = 0;
                    while content[start] != ')' as u8 && loop_count < 3 {
                        second_num_chars.push(content[start]);
                        start += 1;
                        loop_count += 1;
                    }

                    if content[start] as char != ')' {
                        return None;
                    }

                    let second_num_chars = std::str::from_utf8(&second_num_chars).unwrap();
                    let second_num = match second_num_chars.parse::<usize>() {
                        Ok(n) => n,
                        Err(_) => return None,
                    };

                    Some((first_num, second_num))
                }
                _ => None,
            }
        })
        .map(|(f, s)| f * s)
        .sum();

    sum
}
