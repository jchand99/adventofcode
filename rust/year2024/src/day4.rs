pub fn problem_one() -> usize {
    let bytes = std::fs::read_to_string("d4.txt").expect("Input must be readable!");

    let width = bytes
        .lines()
        .nth(0)
        .expect("Input was not empty, expected a line")
        .len();
    let height = bytes.lines().count();

    let bytes = bytes.bytes().filter(|b| *b != b'\n').collect::<Vec<_>>();

    let mut count = 0;
    for i in 0..bytes.len() {
        match bytes[i] {
            b'X' => {
                let x = (i % width) as isize;
                let y = (i / height) as isize;
                // println!("x: {}, y: {}, f: {}", x, y, (y * width as isize) + x,);

                // Left
                if check(x, y, -1, 0, width as isize, height as isize, &bytes) {
                    count += 1;
                }
                // Right
                if check(x, y, 1, 0, width as isize, height as isize, &bytes) {
                    count += 1;
                }
                // Up
                if check(x, y, 0, -1, width as isize, height as isize, &bytes) {
                    count += 1;
                }
                // Down
                if check(x, y, 0, 1, width as isize, height as isize, &bytes) {
                    count += 1;
                }
                // Up-Right
                if check(x, y, 1, -1, width as isize, height as isize, &bytes) {
                    count += 1;
                }
                // Down-Right
                if check(x, y, 1, 1, width as isize, height as isize, &bytes) {
                    count += 1;
                }
                // Up-Left
                if check(x, y, -1, -1, width as isize, height as isize, &bytes) {
                    count += 1;
                }
                // Down-Left
                if check(x, y, -1, 1, width as isize, height as isize, &bytes) {
                    count += 1;
                }
            }
            _ => {}
        }
    }

    count
}

const XMAS: [char; 4] = ['X', 'M', 'A', 'S'];

fn check(
    mut x: isize,
    mut y: isize,
    delta_x: isize,
    delta_y: isize,
    width: isize,
    height: isize,
    bytes: &[u8],
) -> bool {
    let mut xmas = [' '; 4];
    for i in 0..4 {
        xmas[i] = bytes[((y * width) + x) as usize] as char;
        if bytes[((y * width) + x) as usize] as char != XMAS[i] {
            return false;
        }

        if xmas == XMAS {
            return true;
        }

        x += delta_x;
        if x < 0 || x >= width {
            return false;
        }
        y += delta_y;
        if y < 0 || y >= height {
            return false;
        }
    }

    if xmas != XMAS {
        return false;
    }

    true
}

pub fn problem_two() -> usize {
    let bytes = std::fs::read_to_string("d4.txt").expect("Input must be readable!");

    let width = bytes
        .lines()
        .nth(0)
        .expect("Input was not empty, expected a line")
        .len();
    let height = bytes.lines().count();

    let bytes = bytes.bytes().filter(|b| *b != b'\n').collect::<Vec<_>>();

    let mut count = 0;
    for i in 0..bytes.len() {
        match bytes[i] {
            b'A' => {
                let x = (i % width) as isize;
                let y = (i / height) as isize;

                // println!("A");
                if check_crosses(x, y, width as isize, height as isize, &bytes) {
                    count += 1;
                }
            }
            _ => {}
        }
    }

    count
}

fn check_crosses(x: isize, y: isize, width: isize, height: isize, bytes: &[u8]) -> bool {
    let top_left_x = x - 1;
    let top_left_y = y - 1;

    let bottom_right_x = x + 1;
    let bottom_right_y = y + 1;

    if (top_left_x < 0 || top_left_x >= width) || (bottom_right_x < 0 || bottom_right_x >= width) {
        return false;
    }
    if (top_left_y < 0 || top_left_y >= height) || (bottom_right_y < 0 || bottom_right_y >= height)
    {
        return false;
    }

    let top_left = bytes[((top_left_y * width) + top_left_x) as usize] as char;
    let bottom_right = bytes[((bottom_right_y * width) + bottom_right_x) as usize] as char;

    match [top_left, bottom_right] {
        ['S', 'M'] => {}
        ['M', 'S'] => {}
        _ => return false,
    };

    let top_right_x = x + 1;
    let top_right_y = y - 1;

    let bottom_left_x = x - 1;
    let bottom_left_y = y + 1;

    if (top_right_x < 0 || top_right_x >= width) || (bottom_left_x < 0 || bottom_left_x >= width) {
        return false;
    }
    if (top_right_y < 0 || top_right_y >= height) || (bottom_left_y < 0 || bottom_left_y >= height)
    {
        return false;
    }

    let top_right = bytes[((top_right_y * width) + top_right_x) as usize] as char;
    let bottom_left = bytes[((bottom_left_y * width) + bottom_left_x) as usize] as char;

    match [top_right, bottom_left] {
        ['S', 'M'] => {}
        ['M', 'S'] => {}
        _ => return false,
    };

    true
}
