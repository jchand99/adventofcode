// I think this is the ugliest thing I've ever written
// and can be super simplified. I'm also writing this at 12:30 AM
// so try to cut past me some slack lol.
pub fn problem_one() -> usize {
    let count = std::fs::read_to_string("d2.txt")
        .expect("Input must be readable!")
        .lines()
        .filter(|line| {
            line.split_whitespace()
                .flat_map(|a| a.parse::<isize>())
                .zip(
                    line.split_whitespace()
                        .flat_map(|a| a.parse::<isize>())
                        .skip(1),
                )
                .map(|(a, b)| (a - b).abs() < 1 || (a - b).abs() > 3)
                .filter(|&b| b)
                .count()
                == 0
        })
        .filter(|line| {
            let l = line
                .split_whitespace()
                .flat_map(|a| a.parse::<isize>())
                .zip(
                    line.split_whitespace()
                        .flat_map(|a| a.parse::<isize>())
                        .skip(1),
                )
                .fold(vec![], |mut acc, (a, b)| {
                    acc.push(a - b);
                    acc
                });

            l.iter().all(|a| a.is_positive()) || l.iter().all(|a| a.is_negative())
        })
        .count();

    count
}

fn validate(report: &[isize]) -> bool {
    let diff_count = report
        .iter()
        .zip(report.iter().skip(1))
        .map(|(a, b)| (a - b).abs())
        .map(|a| {
            // print!("{},", a);
            a
        })
        .map(|a| a > 3 || a < 1)
        .filter(|&b| b)
        .count();

    let is_sorted = report.is_sorted_by(|a, b| a <= b) || report.is_sorted_by(|a, b| a >= b);
    diff_count == 0 && is_sorted
}

pub fn problem_two() -> usize {
    let count = std::fs::read_to_string("d2.txt")
        .expect("Input must be readable!")
        .lines()
        .filter(|line| {
            let v = line
                .split_whitespace()
                .flat_map(|a| a.parse::<isize>())
                .collect::<Vec<_>>();

            if validate(&v) {
                true
            } else {
                let mut valid = false;
                for i in 0..v.len() {
                    let mut vecc = v.clone();
                    vecc.remove(i);
                    if validate(&vecc) {
                        valid = true;
                        break;
                    }
                }
                valid
            }
        })
        .count();

    count
}
