mod day1;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;
mod day7;

#[test]
fn day1() {
    assert_eq!(day1::problem_one(), 138);
    assert_eq!(day1::problem_two(), 1771);
}

#[test]
fn day2() {
    assert_eq!(day2::problem_one(), 1598415);
    assert_eq!(day2::problem_two(), 3812909);
}

#[test]
fn day3() {
    assert_eq!(day3::problem_one(), 2592);
    assert_eq!(day3::problem_two(), 2360);
}

#[test]
fn day4() {
    assert_eq!(day4::problem_one(), 254575);
    assert_eq!(day4::problem_two(), 1038736);
}

#[test]
fn day5() {
    assert_eq!(day5::problem_one(), 258);
    assert_eq!(day5::problem_two(), 53);
}

#[test]
fn day6() {
    assert_eq!(day6::problem_one(), 377891);
    assert_eq!(day6::problem_two(), 14110788);
}

#[test]
fn day7() {
    assert_eq!(day7::problem_one(), 16076);
    assert_eq!(day7::problem_two(), 2797);
}
