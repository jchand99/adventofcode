use std::collections::HashMap;

#[derive(Debug, Clone, Copy)]
enum Move {
    Left,
    Right,
    Down,
    Up,
}

pub struct Day3 {
    santa_moves: Vec<Move>,
    data: Option<HashMap<(isize, isize), isize>>,
}

pub fn problem_one() -> usize {
    let santa_moves = read_input();
    let mut map = HashMap::new();
    let mut x = 0;
    let mut y = 0;

    map.entry((x, y)).or_insert(1);
    for m in santa_moves {
        match m {
            Move::Up => y += 1,
            Move::Down => y -= 1,
            Move::Right => x += 1,
            Move::Left => x -= 1,
        }

        let i = map.entry((x, y)).or_insert(1);
        *i += 1;
    }

    map.retain(|&_k, &mut v| v >= 1);
    map.len()
}

pub fn problem_two() -> usize {
    let santa_moves = read_input();
    let mut map = HashMap::new();
    let mut sx = 0;
    let mut sy = 0;
    let mut rx = 0;
    let mut ry = 0;

    map.insert((0, 0), 2);
    for (i, m) in santa_moves.iter().enumerate().step_by(2) {
        let rm = santa_moves[i + 1];

        match m {
            Move::Up => sy += 1,
            Move::Down => sy -= 1,
            Move::Right => sx += 1,
            Move::Left => sx -= 1,
        }

        match rm {
            Move::Up => ry += 1,
            Move::Down => ry -= 1,
            Move::Right => rx += 1,
            Move::Left => rx -= 1,
        }

        let si = map.entry((sx, sy)).or_insert(1);
        *si += 1;
        let ri = map.entry((rx, ry)).or_insert(1);
        *ri += 1;
    }

    map.retain(|&_k, &mut v| v >= 1);
    map.len()
}

fn read_input() -> Vec<Move> {
    let input = std::fs::read_to_string("d3.txt").expect("Failed to find input file");
    let mut moves: Vec<Move> = Vec::with_capacity(8200);

    for l in input.lines() {
        for c in l.chars() {
            match c {
                'v' => moves.push(Move::Down),
                '<' => moves.push(Move::Left),
                '>' => moves.push(Move::Right),
                '^' => moves.push(Move::Up),
                _ => {}
            }
        }
    }
    moves
}
