pub fn problem_one() -> i64 {
    let input = std::fs::read_to_string("d1.txt").expect("Failed to find input file.");

    let mut level: i64 = 0;
    for c in input.as_str().chars() {
        match c {
            '(' => level += 1,
            ')' => level -= 1,
            _ => {}
        }
    }

    level
}

pub fn problem_two() -> usize {
    let input = std::fs::read_to_string("d1.txt").expect("Failed to find input file.");

    let mut level = 0;
    let mut answer = usize::MAX;
    for (i, c) in input.as_str().chars().enumerate() {
        match c {
            '(' => level += 1,
            ')' => level -= 1,
            _ => {}
        }

        if level < 0 {
            if i + 1 < answer {
                answer = i + 1;
            }
        }
    }

    answer
}
