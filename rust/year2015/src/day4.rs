pub fn problem_one() -> usize {
    let mut index: usize = 0;
    loop {
        let seed = format!("bgvyzdsv{}", index);
        let digest = md5::compute(seed.as_bytes());

        let value = convert_digest_to_u128(digest.to_vec());

        if value <= 0x00000fffffffffffffffffffffffffff {
            break;
        }

        index += 1;
    }
    index
}

pub fn problem_two() -> usize {
    let mut index: usize = 0;
    loop {
        let seed = format!("bgvyzdsv{}", index);
        let digest = md5::compute(seed.as_bytes());

        let value = convert_digest_to_u128(digest.to_vec());

        if value <= 0x000000ffffffffffffffffffffffffff {
            break;
        }

        index += 1;
    }
    index
}

fn convert_digest_to_u128(v: Vec<u8>) -> u128 {
    (v[0] as u128) << 0xf * 8
        | (v[1] as u128) << 0xe * 8
        | (v[2] as u128) << 0xd * 8
        | (v[3] as u128) << 0xc * 8
        | (v[4] as u128) << 0xb * 8
        | (v[5] as u128) << 0xa * 8
        | (v[6] as u128) << 0x9 * 8
        | (v[7] as u128) << 0x8 * 8
        | (v[8] as u128) << 0x7 * 8
        | (v[9] as u128) << 0x6 * 8
        | (v[10] as u128) << 0x5 * 8
        | (v[11] as u128) << 0x4 * 8
        | (v[12] as u128) << 0x3 * 8
        | (v[13] as u128) << 0x2 * 8
        | (v[14] as u128) << 0x1 * 8
        | (v[15] as u128) << 0x0 * 8
}
