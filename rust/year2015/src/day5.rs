use fancy_regex::Regex;

pub fn problem_one() -> usize {
    let nr: Vec<Regex> = vec![
        Regex::new("[aeiou].*[aeiou].*[aeiou]").unwrap(),
        Regex::new("([a-z])\\1").unwrap(),
        Regex::new("ab|cd|pq|xy").unwrap(),
    ];

    let input = std::fs::read_to_string("d5.txt").expect("Failed to find input file");
    let mut lines = input.lines().collect::<Vec<_>>();

    lines.retain(|l| nr[0].is_match(l).unwrap());
    lines.retain(|l| nr[1].is_match(l).unwrap());
    lines.retain(|l| !nr[2].is_match(l).unwrap());

    lines.len()
}

pub fn problem_two() -> usize {
    let nr2: Vec<Regex> = vec![
        Regex::new("([a-z][a-z])[a-z].*\\1|([a-z][a-z])\\2").unwrap(),
        Regex::new("([a-z])[a-z]\\1").unwrap(),
    ];

    let input = std::fs::read_to_string("d5.txt").expect("Failed to find input file");
    let mut lines = input.lines().collect::<Vec<_>>();

    lines.retain(|l| nr2[0].is_match(l).unwrap());
    lines.retain(|l| nr2[1].is_match(l).unwrap());

    lines.len()
}
