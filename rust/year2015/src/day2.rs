struct Pres {
    pub volume: i32,
    pub surface_area: i32,
    pub smallest_side_area: i32,
    pub smallest_side_perim: i32,
}

impl Pres {
    pub fn new(
        volume: i32,
        surface_area: i32,
        smallest_side_area: i32,
        smallest_side_perim: i32,
    ) -> Self {
        Self {
            volume,
            surface_area,
            smallest_side_area,
            smallest_side_perim,
        }
    }
}

fn least(min: i32, u: i32, v: i32) -> bool {
    min <= u && min <= v
}

fn min_of_three(a: i32, b: i32, c: i32) -> i32 {
    if least(a, b, c) {
        return a;
    }
    if least(b, a, c) {
        return b;
    }
    if least(c, a, b) {
        return c;
    } else {
        a
    }
}

pub fn problem_one() -> i32 {
    let data = read_input();
    data.iter()
        .fold(0, |acc, e| acc + e.smallest_side_area + e.surface_area)
}

pub fn problem_two() -> i32 {
    let data = read_input();
    data.iter()
        .fold(0, |acc, e| acc + e.volume + e.smallest_side_perim)
}

fn read_input() -> Vec<Pres> {
    let input = std::fs::read_to_string("d2.txt").expect("Failed to find input file");
    let mut data: Vec<Pres> = Vec::with_capacity(1000);

    for l in input.lines() {
        let a = l
            .split('x')
            .map(|c| str::parse::<i32>(c).unwrap())
            .collect::<Vec<i32>>();

        let v = Pres::new(
            a[0] * a[1] * a[2],
            (2 * a[0] * a[1]) + (2 * a[1] * a[2]) + (2 * a[2] * a[0]),
            min_of_three(a[0] * a[1], a[1] * a[2], a[2] * a[0]),
            min_of_three(
                a[0] + a[0] + a[1] + a[1],
                a[1] + a[1] + a[2] + a[2],
                a[0] + a[0] + a[2] + a[2],
            ),
        );

        data.push(v);
    }

    data
}
