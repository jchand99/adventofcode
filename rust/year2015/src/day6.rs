#[derive(Clone, Copy, Debug)]
enum LightState {
    On(u32),
    Off,
}

#[derive(Clone, Copy, Debug)]
enum Instruction {
    TurnOn(u16, u16, u16, u16),
    TurnOff(u16, u16, u16, u16),
    Toggle(u16, u16, u16, u16),
}

struct Data {
    lights: Vec<LightState>,
    instructions: Vec<Instruction>,
}

pub fn problem_one() -> i32 {
    let mut data = read_input();
    for i in data.instructions {
        match i {
            Instruction::Toggle(x, y, xx, yy) => {
                for i in x..=xx {
                    for j in y..=yy {
                        let i = i as usize;
                        let j = j as usize;
                        let l = &mut data.lights[i + j * 1000];

                        match l {
                            LightState::On(_) => {
                                *l = LightState::Off;
                            }
                            LightState::Off => {
                                *l = LightState::On(1);
                            }
                        }
                    }
                }
            }
            Instruction::TurnOn(x, y, xx, yy) => {
                for i in x..=xx {
                    for j in y..=yy {
                        let i = i as usize;
                        let j = j as usize;
                        data.lights[i + j * 1000] = LightState::On(1);
                    }
                }
            }
            Instruction::TurnOff(x, y, xx, yy) => {
                for i in x..=xx {
                    for j in y..=yy {
                        let i = i as usize;
                        let j = j as usize;
                        data.lights[i + j * 1000] = LightState::Off;
                    }
                }
            }
        }
    }

    let count = data.lights.iter().fold(0, |mut acc, l| match l {
        LightState::On(_) => {
            acc += 1;
            acc
        }
        LightState::Off => acc,
    });

    count
}

pub fn problem_two() -> u32 {
    let mut data = read_input();
    for i in data.instructions {
        match i {
            Instruction::Toggle(x, y, xx, yy) => {
                for i in x..=xx {
                    for j in y..=yy {
                        let i = i as usize;
                        let j = j as usize;
                        let l = &mut data.lights[i + j * 1000];

                        match l {
                            LightState::On(v) => {
                                *l = LightState::On(*v + 2);
                            }
                            LightState::Off => {
                                *l = LightState::On(2);
                            }
                        }
                    }
                }
            }
            Instruction::TurnOn(x, y, xx, yy) => {
                for i in x..=xx {
                    for j in y..=yy {
                        let i = i as usize;
                        let j = j as usize;
                        if let LightState::On(v) = data.lights[i + j * 1000] {
                            data.lights[i + j * 1000] = LightState::On(v + 1);
                        } else {
                            data.lights[i + j * 1000] = LightState::On(1);
                        }
                    }
                }
            }
            Instruction::TurnOff(x, y, xx, yy) => {
                for i in x..=xx {
                    for j in y..=yy {
                        let i = i as usize;
                        let j = j as usize;
                        if let LightState::On(v) = data.lights[i + j * 1000] {
                            if v == 0 {
                                data.lights[i + j * 1000] = LightState::Off;
                            } else {
                                data.lights[i + j * 1000] = LightState::On(v - 1);
                            }
                        }
                    }
                }
            }
        }
    }

    let count = data.lights.iter().fold(0, |mut acc, l| match l {
        LightState::On(v) => {
            acc += v;
            acc
        }
        LightState::Off => acc,
    });

    count
}

fn read_input() -> Data {
    let content = std::fs::read_to_string("d6.txt").expect("Failed to find input file");
    let mut instructions = Vec::with_capacity(300);

    for l in content.lines() {
        let parts = l.split("through").map(|p| p.trim()).collect::<Vec<_>>();
        let to = parts[1];

        let (xx, yy) = to.split_once(",").unwrap();
        let xx = str::parse::<u16>(xx).unwrap();
        let yy = str::parse::<u16>(yy).unwrap();

        let ins_and_from = parts[0];
        let (command, xy) = ins_and_from.rsplit_once(" ").unwrap();

        let (x, y) = xy.split_once(",").unwrap();
        let x = str::parse::<u16>(x).unwrap();
        let y = str::parse::<u16>(y).unwrap();

        let inst = match command {
            "turn off" => Instruction::TurnOff(x, y, xx, yy),
            "turn on" => Instruction::TurnOn(x, y, xx, yy),
            "toggle" => Instruction::Toggle(x, y, xx, yy),
            _ => panic!("Incorrect command!"),
        };

        instructions.push(inst);
    }

    let mut lights = Vec::with_capacity(1000 * 1000);
    for _ in 0..(1000 * 1000) {
        lights.push(LightState::Off);
    }

    Data {
        lights,
        instructions,
    }
}
