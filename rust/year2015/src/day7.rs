use std::collections::{HashMap, VecDeque};

#[derive(Debug)]
enum Op<'a> {
    And(&'a str, &'a str, &'a str),
    Vand(u16, &'a str, &'a str),
    Map(&'a str, &'a str),
    Or(&'a str, &'a str, &'a str),
    Not(&'a str, &'a str),
    Rshift(&'a str, u16, &'a str),
    Lshift(&'a str, u16, &'a str),
}

pub fn problem_one() -> u16 {
    let mut map: HashMap<&str, u16> = HashMap::new();
    let input = std::fs::read_to_string("d7.txt").expect("Failed to find input file");

    operate(&mut map, &input);

    map["a"]
}

pub fn problem_two() -> u16 {
    let mut map: HashMap<&str, u16> = HashMap::new();
    map.insert("b", 16076);
    let input = std::fs::read_to_string("d7.txt").expect("Failed to find input file");

    operate(&mut map, &input);

    map["a"]
}

fn operate<'a, 'b>(map: &'a mut HashMap<&'b str, u16>, input: &'b str) {
    let mut operations: VecDeque<Op> = VecDeque::new();

    for line in input.lines() {
        let s = line.split_whitespace().collect::<Vec<_>>();

        match s.len() {
            3 => match s[0].parse::<u16>() {
                Ok(v) => {
                    map.entry(s[2]).or_insert(v);
                }
                Err(_) => operations.push_back(Op::Map(s[0], s[2])),
            },
            5 => match s[1] {
                "AND" => match s[0].parse::<u16>() {
                    Ok(v) => operations.push_back(Op::Vand(v, s[2], s[4])),
                    Err(_) => operations.push_back(Op::And(s[0], s[2], s[4])),
                },
                "OR" => operations.push_back(Op::Or(s[0], s[2], s[4])),
                "LSHIFT" => {
                    let v = s[2]
                        .parse::<u16>()
                        .expect("Failed to parse left shift value");
                    operations.push_back(Op::Lshift(s[0], v, s[4]))
                }
                "RSHIFT" => {
                    let v = s[2]
                        .parse::<u16>()
                        .expect("Failed to parse right shift value");
                    operations.push_back(Op::Rshift(s[0], v, s[4]))
                }
                _ => panic!("Cannot run operation!"),
            },
            4 => operations.push_back(Op::Not(s[1], s[3])),
            _ => panic!("too many or not enough arguments"),
        }
    }

    while let Some(op) = operations.pop_front() {
        match op {
            Op::And(reg1, reg2, o_reg) => {
                let Some(reg1) = map.get(reg1) else { operations.push_back(op); continue; /* move to next operation, put this one in back*/ };
                let Some(reg2) = map.get(reg2) else { operations.push_back(op); continue; };
                let o = *reg1 & *reg2;
                map.entry(o_reg).or_insert(o);
            }
            Op::Vand(value, reg2, o_reg) => {
                let Some(reg2) = map.get(reg2) else { operations.push_back(op); continue; };
                let o = value & *reg2;
                map.entry(o_reg).or_insert(o);
            }
            Op::Map(reg1, reg2) => {
                let Some(reg1) = map.get(reg1) else { operations.push_back(op); continue; };
                let reg1 = reg1.clone();

                map.entry(reg2).and_modify(|e| *e = reg1).or_insert(reg1);
            }
            Op::Or(reg1, reg2, o_reg) => {
                let Some(reg1) = map.get(reg1) else { operations.push_back(op); continue; };
                let Some(reg2) = map.get(reg2) else { operations.push_back(op); continue; };
                let o = *reg1 | *reg2;
                map.entry(o_reg).or_insert(o);
            }
            Op::Lshift(reg1, value, o_reg) => {
                let Some(reg1) = map.get(reg1) else { operations.push_back(op); continue; };
                let o = *reg1 << value;
                map.entry(o_reg).or_insert(o);
            }
            Op::Rshift(reg1, value, o_reg) => {
                let Some(reg1) = map.get(reg1) else { operations.push_back(op); continue; };
                let o = *reg1 >> value;
                map.entry(o_reg).or_insert(o);
            }
            Op::Not(reg1, reg2) => {
                let Some(reg1) = map.get(reg1) else { operations.push_back(op); continue; };
                let o = !reg1;
                map.entry(reg2).or_insert(o);
            }
        }
    }
}
