#[derive(Debug, Clone)]
struct Passport {
    pub byr: bool, // (Birth Year)
    pub iyr: bool, // (Issue Year)
    pub eyr: bool, // (Expiration Year)
    pub hgt: bool, // (Height)
    pub hcl: bool, // (Hair Color)
    pub ecl: bool, // (Eye Color)
    pub pid: bool, // (Passport ID)
    pub cid: bool, // (Country ID)
}

impl Passport {
    pub fn is_valid_p1(&self) -> bool {
        self.byr && self.iyr && self.eyr && self.hgt && self.hcl && self.ecl && self.pid
    }
}

impl Default for Passport {
    fn default() -> Self {
        Passport {
            byr: false,
            iyr: false,
            eyr: false,
            hgt: false,
            hcl: false,
            ecl: false,
            pid: false,
            cid: false,
        }
    }
}

pub fn problem_one() -> i32 {
    let input = std::fs::read_to_string("d4.txt").expect("Failed to find input file.");
    let mut total = 0;
    let mut pp = Passport::default();
    for line in input.lines() {
        if line.is_empty() {
            if pp.is_valid_p1() {
                total += 1;
            }

            pp = Passport::default();
        }

        line.split_whitespace().for_each(|v| {
            v.split(':').for_each(|vv| match vv {
                "byr" => pp.byr = true,
                "iyr" => pp.iyr = true,
                "eyr" => pp.eyr = true,
                "hgt" => pp.hgt = true,
                "hcl" => pp.hcl = true,
                "ecl" => pp.ecl = true,
                "pid" => pp.pid = true,
                "cid" => pp.cid = true,
                _ => {}
            })
        });
    }

    if pp.is_valid_p1() {
        total += 1;
    }

    total
}

#[derive(Debug)]
struct Passport2 {
    pub byr: usize,          // (Birth Year)
    pub iyr: usize,          // (Issue Year)
    pub eyr: usize,          // (Expiration Year)
    pub hgt: Height,         // (Height)
    pub hcl: String,         // (Hair Color)
    pub ecl: Option<String>, // (Eye Color)
    pub pid: String,         // (Passport ID)
    pub cid: Option<String>, // (Country ID)
}

impl Default for Passport2 {
    fn default() -> Self {
        Self {
            byr: 0,
            iyr: 0,
            eyr: 0,
            hgt: Height::None,
            hcl: "".to_string(),
            ecl: None,
            pid: "".to_string(),
            cid: None,
        }
    }
}

impl Passport2 {
    fn is_valid(&self) -> bool {
        (self.byr >= 1920 && self.byr <= 2002)
            && (self.iyr >= 2010 && self.iyr <= 2020)
            && (self.eyr >= 2020 && self.eyr <= 2030)
            && (self.hgt.is_valid())
            && (self.hcl.starts_with('#')/* && only contains alpha numerics */)
            && (self.ecl.is_some())
            && (self.pid.len() == 9 && self.pid.parse::<usize>().is_ok())
    }
}

#[derive(Debug)]
enum Height {
    Centimeters(usize),
    Inches(usize),
    None,
}

impl Height {
    fn is_valid(&self) -> bool {
        match self {
            Self::Centimeters(h) => *h >= 150 && *h <= 193,
            Self::Inches(h) => *h >= 59 && *h <= 76,
            Self::None => false,
        }
    }
}

pub fn problem_two() -> i32 {
    let input = std::fs::read_to_string("d4.txt").expect("Failed to find input file.");
    let mut total = 0;
    let mut pp = Passport2::default();
    for line in input.lines() {
        if line.is_empty() {
            if pp.is_valid() {
                total += 1;
            }

            pp = Passport2::default();
        }

        line.split_whitespace().for_each(|v| {
            v.split(':')
                .zip(v.split(':').skip(1).step_by(2))
                .for_each(|(left, right)| match left {
                    "byr" => pp.byr = right.parse::<usize>().unwrap(),
                    "iyr" => pp.iyr = right.parse::<usize>().unwrap(),
                    "eyr" => pp.eyr = right.parse::<usize>().unwrap(),
                    "hgt" => {
                        pp.hgt = {
                            if right.contains("in") {
                                Height::Inches(
                                    right.trim_end_matches("in").parse::<usize>().unwrap(),
                                )
                            } else if right.contains("cm") {
                                Height::Centimeters(
                                    right.trim_end_matches("cm").parse::<usize>().unwrap(),
                                )
                            } else {
                                Height::None
                            }
                        }
                    }
                    "hcl" => pp.hcl = right.to_owned(),
                    "ecl" => {
                        pp.ecl = match right {
                            "amb" => Some(right.to_owned()),
                            "blu" => Some(right.to_owned()),
                            "brn" => Some(right.to_owned()),
                            "gry" => Some(right.to_owned()),
                            "grn" => Some(right.to_owned()),
                            "hzl" => Some(right.to_owned()),
                            "oth" => Some(right.to_owned()),
                            _ => None,
                        }
                    }
                    "pid" => pp.pid = right.to_owned(),
                    "cid" => pp.cid = Some(right.to_owned()),
                    _ => {}
                })
        });
    }

    if pp.is_valid() {
        total += 1;
    }

    total
}
