#[derive(Debug, Clone, Copy)]
pub enum OpCode {
    Nop(isize),
    Acc(isize),
    Jmp(isize),
}

fn read_ops() -> Vec<(u8, OpCode)> {
    let input = std::fs::read_to_string("d8.txt").expect("Failed to find input file.");
    input
        .lines()
        .map(|line| {
            if let Some((code, value)) = line.split_once(' ') {
                let value = value.parse::<isize>().unwrap();

                match code {
                    "nop" => (0, OpCode::Nop(value)),
                    "acc" => (0, OpCode::Acc(value)),
                    "jmp" => (0, OpCode::Jmp(value)),
                    _ => panic!("Invalid op code"),
                }
            } else {
                panic!("Invalid loc");
            }
        })
        .collect::<Vec<_>>()
}

pub fn problem_one() -> isize {
    let mut ops = read_ops();
    let mut idx: isize = 0;
    let mut acc = 0;
    loop {
        let (c, op) = &mut ops[idx as usize];
        if *c == 1 {
            break;
        }

        match op {
            OpCode::Nop(_) => {
                idx += 1;
            }
            OpCode::Acc(v) => {
                acc += *v;
                idx += 1;
            }
            OpCode::Jmp(v) => {
                idx += *v;
            }
        }

        *c += 1;
    }

    acc
}

pub fn problem_two() -> isize {
    let ops = read_ops();

    let jmps = ops
        .iter()
        .enumerate()
        .filter(|(_, &op)| match op.1 {
            OpCode::Jmp(_) => true,
            _ => false,
        })
        .map(|(id, _)| id)
        .collect::<Vec<_>>();

    for i in jmps {
        let mut ops = ops.clone();
        let v = match ops[i].1 {
            OpCode::Jmp(v) => v,
            _ => unreachable!(),
        };

        ops[i] = (0, OpCode::Nop(v));

        let (acc, b) = run_ops(ops);
        if b {
            return acc;
        }
    }

    0
}

fn run_ops(mut ops: Vec<(u8, OpCode)>) -> (isize, bool) {
    let mut idx: isize = 0;
    let mut acc = 0;
    loop {
        if idx as usize >= ops.len() {
            return (acc, true);
        }

        let (c, op) = &mut ops[idx as usize];
        if *c == 2 {
            return (acc, false);
        }

        match op {
            OpCode::Nop(_v) => {
                idx += 1;
            }
            OpCode::Acc(v) => {
                idx += 1;
                acc += *v;
            }
            OpCode::Jmp(v) => {
                idx += *v;
            }
        }

        *c += 1;
    }
}
