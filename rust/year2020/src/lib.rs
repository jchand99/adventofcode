#![allow(dead_code)]

// mod day1;
mod day10;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;
mod day7;
mod day8;
mod day9;

// #[test]
// fn day1() {
//     assert_eq!(day1::problem_one(), 138);
//     assert_eq!(day1::problem_two(), 1771);
// }

#[test]
fn day2() {
    assert_eq!(day2::problem_one(), 655);
    assert_eq!(day2::problem_two(), 673);
}

#[test]
fn day3() {
    assert_eq!(day3::problem_one(), 205);
    assert_eq!(day3::problem_two(), 3952146825);
}

#[test]
fn day4() {
    assert_eq!(day4::problem_one(), 219);
    assert_eq!(day4::problem_two(), 127);
}

#[test]
fn day5() {
    assert_eq!(day5::problem_one(), 901);
    assert_eq!(day5::problem_two(), 661);
}

#[test]
fn day6() {
    assert_eq!(day6::problem_one(), 6680);
    assert_eq!(day6::problem_two(), 3117);
}

#[test]
fn day7() {
    assert_eq!(day7::problem_one(), 224);
    assert_eq!(day7::problem_two(), 1488);
}

#[test]
fn day8() {
    assert_eq!(day8::problem_one(), 1810);
    assert_eq!(day8::problem_two(), 969);
}

#[test]
fn day9() {
    assert_eq!(day9::problem_one(), 26134589);
    assert_eq!(day9::problem_two(), 3535124);
}

#[test]
fn day10() {
    assert_eq!(day10::problem_one(), 2376);
    assert_eq!(day10::problem_two(), 129586085429248);
}
