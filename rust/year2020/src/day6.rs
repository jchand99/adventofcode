pub fn problem_one() -> isize {
    let input = std::fs::read_to_string("d6.txt").expect("Failed to find input file.");
    let mut tot = 0;
    let mut cache = [false; 26];
    for line in input.lines() {
        if line.is_empty() {
            tot += cache.iter().fold(0, |mut acc, n| {
                if *n {
                    acc += 1;
                }
                acc
            });
            cache = [false; 26];
        }

        for c in line.chars() {
            let idx = (c as u8 - b'a') as usize;
            if !cache[idx] {
                cache[idx] = true;
            }
        }
    }

    tot += cache.iter().fold(0, |mut acc, n| {
        if *n {
            acc += 1;
        }
        acc
    });

    tot
}

pub fn problem_two() -> isize {
    let input = std::fs::read_to_string("d6.txt").expect("Failed to find input file.");
    let mut tot = 0;
    let mut cache = [0; 26];
    let mut person_count = 0;
    for line in input.lines() {
        if line.is_empty() {
            let a = cache.iter().fold(0, |mut acc, n| {
                if *n == person_count {
                    acc += 1;
                }
                acc
            });

            tot += a;
            person_count = 0;
            cache = [0; 26];
            continue;
        }
        person_count += 1;

        for c in line.chars() {
            let idx = (c as u8 - b'a') as usize;
            cache[idx] += 1;
        }
    }

    tot += cache.iter().fold(0, |mut acc, n| {
        if *n == person_count {
            acc += 1;
        }
        acc
    });

    tot
}
