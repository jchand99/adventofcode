use std::cmp::Ordering;

pub fn problem_one() -> usize {
    let input = std::fs::read_to_string("d9.txt").expect("Failed to find input file.");
    let nums = input
        .lines()
        .map(|l| l.parse::<usize>().unwrap())
        .collect::<Vec<_>>();

    let preamble_length = 25;

    let mut start_idx = preamble_length;
    let mut start_window = &nums[(start_idx - preamble_length)..start_idx];

    loop {
        let mut value = nums[start_idx];
        let mut found = false;

        for n in start_window {
            if *n > value {
                continue;
            }

            let compliment = value - n;
            if contains(start_window, compliment, *n) {
                // println!("CONTAINS");
                start_idx += 1;
                value = nums[start_idx];
                start_window = &nums[(start_idx - preamble_length)..start_idx];
                found = true;
            }

            if found {
                break;
            }
        }

        if !found {
            break value;
        }
    }
}

fn contains(haystack: &[usize], needle: usize, o: usize) -> bool {
    for n in haystack {
        if needle == *n && *n != o {
            return true;
        }
    }

    false
}

pub fn problem_two() -> usize {
    let input = std::fs::read_to_string("d9.txt").expect("Failed to find input file.");
    let nums = input
        .lines()
        .map(|l| l.parse::<usize>().unwrap())
        .collect::<Vec<_>>();

    let mut i = 0;
    let mut j = 1;

    loop {
        let s = nums[i..j].iter().sum::<usize>();
        match s.cmp(&26134589) {
            Ordering::Less => {
                j += 1;
            }
            Ordering::Equal => break,
            Ordering::Greater => {
                i += 1;
                j = i + 1;
            }
        }
    }

    let mut a = nums[i..j].to_vec();
    a.sort_unstable();
    a[0] + a[a.len() - 1]
}
