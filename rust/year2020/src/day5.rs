pub fn problem_one() -> isize {
    let input = std::fs::read_to_string("d5.txt").expect("Failed to find input file.");

    let mut high = i32::MIN;
    for line in input.lines() {
        let line = line.as_bytes();
        let seat_id =
            (pinpoint(0, 127, &line[0..7]) as i32 * 8) + pinpoint(0, 7, &line[7..]) as i32;

        if seat_id > high {
            high = seat_id
        }
    }

    high as isize
}

fn pinpoint(lo: u8, hi: u8, s: &[u8]) -> u8 {
    // println!("lo: {lo}, hi: {hi}");
    if s.len() == 1 {
        return match s[0] {
            b'F' | b'L' => lo,
            b'B' | b'R' => hi,
            _ => panic!("Cannot be letter other than F, B, L or R"),
        };
    }

    match s[0] {
        b'F' | b'L' => pinpoint(
            lo,
            hi - f32::ceil((hi as f32 - lo as f32) / 2.0) as u8,
            &s[1..],
        ),
        b'B' | b'R' => pinpoint(
            lo + f32::ceil((hi as f32 - lo as f32) / 2.0) as u8,
            hi,
            &s[1..],
        ),
        _ => panic!("Cannot be letter other than F, B, L or R"),
    }
}

pub fn problem_two() -> isize {
    let max = problem_one() as usize;
    let mut v: Vec<bool> = vec![false; max + 1];
    let mut ans = 0;

    let input = std::fs::read_to_string("d5.txt").expect("Failed to find input file.");
    for line in input.lines() {
        let line = line.as_bytes();
        v[(pinpoint(0, 127, &line[0..7]) as i32 * 8) as usize
            + pinpoint(0, 7, &line[7..]) as usize] = true;
    }

    match v.iter().enumerate().filter(|(_i, b)| **b == false).last() {
        Some((v, _)) => ans = v as isize,
        None => {}
    }

    ans
}
