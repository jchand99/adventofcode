#[derive(Debug)]
struct PasswordDetails {
    lo: usize,
    hi: usize,
    ch: char,
}

pub fn problem_one() -> i32 {
    let input = std::fs::read_to_string("d2.txt").expect("Failed to find input file.");
    input
        .lines()
        .map(|l| {
            let s = l.split_whitespace().collect::<Vec<_>>();
            let v = s[0]
                .split('-')
                .map(|v| v.parse::<usize>().expect("Failed to parse."))
                .collect::<Vec<_>>();

            (
                PasswordDetails {
                    lo: v[0],
                    hi: v[1],
                    ch: s[1].chars().nth(0).expect("No char found"),
                },
                s[2],
            )
        })
        .fold(0, |mut acc, (pwd, pw)| {
            let mut count = 0;
            for c in pw.chars() {
                if c == pwd.ch {
                    count += 1;
                }
            }

            if count <= pwd.hi && count >= pwd.lo {
                acc += 1;
            }

            acc
        })
}

pub fn problem_two() -> i32 {
    let input = std::fs::read_to_string("d2.txt").expect("Failed to find input file.");
    input
        .lines()
        .map(|l| {
            let s = l.split_whitespace().collect::<Vec<_>>();
            let v = s[0]
                .split('-')
                .map(|v| v.parse::<usize>().expect("Failed to parse"))
                .collect::<Vec<_>>();

            let ch = s[1].chars().nth(0).expect("No char found");

            (v[0], v[1], ch, s[2])
        })
        .fold(0, |mut acc, (lo, hi, ch, pw)| {
            let loc = pw.chars().nth(lo - 1).expect("Failed to get lo char");
            let hic = pw.chars().nth(hi - 1).expect("Failed to get hi char");
            if loc == ch && hic != ch {
                acc += 1
            } else if loc != ch && hic == ch {
                acc += 1
            }

            acc
        })
}
