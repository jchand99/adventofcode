use std::collections::HashMap;

pub fn problem_one() -> usize {
    let input = std::fs::read_to_string("d10.txt").expect("Failed to find input file.");

    let mut nums = input
        .lines()
        .map(str::parse)
        .map(|r| r.unwrap())
        .collect::<Vec<usize>>();

    nums.sort_unstable();

    let mut d1 = 1;
    let mut d3 = 1;

    nums.iter().zip(nums.iter().skip(1)).for_each(|(i, j)| {
        if j - i == 1 {
            d1 += 1;
        }
        if j - i == 3 {
            d3 += 1;
        }
    });

    d1 * d3
}

pub fn problem_two() -> usize {
    // dis one gon be hard
    // needs some sort of formula
    // since value will be more dan a tril

    let input = std::fs::read_to_string("d10.txt").expect("Failed to find input file.");

    let mut nums = input
        .lines()
        .map(str::parse)
        .map(|r| r.unwrap())
        .collect::<Vec<usize>>();

    nums.insert(0, 0);
    nums.sort_unstable();

    let mut mem = HashMap::new();
    permutations(0, &nums, &mut mem)
}

fn permutations(n: usize, nums: &Vec<usize>, mem: &mut HashMap<usize, usize>) -> usize {
    // Base case, if we reach the end of the list of nums then that is the last
    // permutation.
    if n >= nums.len() - 1 {
        return 1;
    }

    // If the permutation is in the memory, use that count instead of calculating
    if let Some(&v) = mem.get(&n) {
        v
    } else {
        let mut v = 0;

        // If the next few numbers in the list have a diff between 1 and 3
        // calculate those three permutations
        for j in (n + 1)..nums.len() {
            if nums[j] - nums[n] <= 3 {
                v += permutations(j, nums, mem);
            }
        }

        mem.insert(n, v);
        v
    }
}
