pub fn problem_one() -> i32 {
    let input = std::fs::read_to_string("d3.txt").expect("Failed to find input file.");
    let lines = input.lines().map(|l| l.as_bytes()).collect::<Vec<_>>();

    tree_bonk(&lines, 3, 1)
}

pub fn problem_two() -> i128 {
    let input = std::fs::read_to_string("d3.txt").expect("Failed to find input file.");
    let lines = input.lines().map(|l| l.as_bytes()).collect::<Vec<_>>();

    tree_bonk(&lines, 1, 1) as i128
        * tree_bonk(&lines, 3, 1) as i128
        * tree_bonk(&lines, 5, 1) as i128
        * tree_bonk(&lines, 7, 1) as i128
        * tree_bonk(&lines, 1, 2) as i128
}

fn tree_bonk(lines: &Vec<&[u8]>, right: usize, down: usize) -> i32 {
    let mut total = 0;
    let mut x = 0;
    let width = lines[0].len();

    for line in lines.iter().skip(down).step_by(down) {
        x = (x + right) % width;
        if line[x] == b'#' {
            total += 1;
        }
    }

    total
}
