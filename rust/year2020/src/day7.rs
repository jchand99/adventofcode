use std::collections::HashMap;

pub fn problem_one() -> isize {
    let input = std::fs::read_to_string("d7.txt").expect("Failed to find input file.");
    let mut map: HashMap<&str, Vec<&str>> = HashMap::new();
    for line in input.lines() {
        let line = line.trim_end_matches('.');
        if let Some((left, right)) = line.split_once("contain") {
            right.trim().split(',').for_each(|bag| match bag {
                "no other bags" => {}
                _ => {
                    let e = map
                        .entry(&bag.trim().trim_end_matches('s')[2..])
                        .or_insert(vec![]);
                    e.push(left.trim().trim_end_matches('s'));
                }
            });
        }
    }

    let mut target_bags = Vec::new();
    collect_bags(&mut target_bags, &map, "shiny gold bag");

    target_bags.len() as isize
}

fn collect_bags<'a>(
    target_bags: &mut Vec<&'a str>,
    map: &HashMap<&'a str, Vec<&'a str>>,
    bag: &'a str,
) {
    if let Some(bags) = map.get(bag) {
        if bags.len() == 0 {
            return;
        }

        for b in bags {
            if !target_bags.contains(b) {
                target_bags.push(b);
            }

            collect_bags(target_bags, map, b);
        }
    } else {
        return;
    }
}

pub fn problem_two() -> usize {
    let input = std::fs::read_to_string("d7.txt").expect("Failed to find input file.");
    let mut map: HashMap<&str, Vec<(usize, &str)>> = HashMap::new();
    for line in input.lines() {
        let line = line.trim_end_matches('.');
        if let Some((left, right)) = line.split_once("contain") {
            right.trim().split(',').for_each(|bag| match bag {
                "no other bags" => {}
                _ => {
                    let e = map
                        .entry(&left.trim().trim_end_matches('s'))
                        .or_insert(vec![]);
                    let bag = bag.trim().trim_end_matches('s');
                    e.push((bag[..1].parse::<usize>().unwrap(), &bag[2..]));
                }
            });
        }
    }

    count_bags(&map, "shiny gold bag")
}

fn count_bags(map: &HashMap<&str, Vec<(usize, &str)>>, bag: &str) -> usize {
    if let Some(bags) = map.get(bag) {
        if bags.len() == 0 {
            return 0;
        }

        let mut tot = 0;
        for b in bags {
            tot += b.0 + (b.0 * count_bags(map, b.1));
        }

        return tot;
    } else {
        return 0;
    }
}
