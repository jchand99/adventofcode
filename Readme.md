This is a collection of all my advent of code attempts in the languages I've used.

Each language has it's own folder, inside is the setup for each year I used that language for.
